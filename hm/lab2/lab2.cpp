﻿#include <opencv2/highgui/highgui.hpp>
#include <iostream>

using namespace cv;
using namespace std;

int main() {
	Mat img1 = Mat::zeros(120, 768, CV_8UC1);
	for (int i = 3; i < img1.cols; i += 3)
	{
		img1(Rect(i, 0, 768 - i, 60)) += 1;
	}
	img1.convertTo(img1, CV_32FC1, 1.0 / 255.0);
	Rect rect1 = Rect(0, 0, 768, 60);
	Rect rect2 = Rect(0, 60, 768, 60);
	pow(img1(rect1), 2.4, img1(rect2));
	img1.convertTo(img1, CV_8UC1, 255.0);
	imwrite("result1.jpg", img1);
	Mat img2 = Mat::zeros(120, 768, CV_8UC1);
	img2(Rect(0, 0, 768, 60)) = 5;
	for (int i = 30; i < img2.cols; i += 30)
	{
		img2(Rect(i, 0, 768 - i, 60)) += 10;
	}
	img2.convertTo(img2, CV_32FC1, 1.0 / 255.0);
	pow(img2(rect1), 2.4, img2(rect2));
	img2.convertTo(img2, CV_8UC1, 255.0);
	imwrite("result2.jpg", img2);
	waitKey(0);
	return 0;
}