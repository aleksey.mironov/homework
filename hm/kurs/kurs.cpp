﻿#include "opencv2/imgcodecs.hpp"
#include "opencv2/highgui.hpp"
#include "opencv2/imgproc.hpp"
#include <iostream>
#include <string>
#include "opencv2/imgproc.hpp"
#include <opencv2/dnn.hpp>
#include <iostream>
#include "opencv2/opencv.hpp"
#include "opencv2/core/core.hpp"
#include <vector>
using namespace std;
using namespace cv;

void readme(string& message)
{
	cout << message << endl;
}

float s(int x1, int y1, int x2, int y2, int x3, int y3, int x4, int y4)
{
	int left = std::max(x1, x3);
	int top = std::min(y2, y4);
	int right = std::min(x2, x4);
	int bottom = std::max(y1, y3);
	int width = right - left;
	int height = top - bottom;
	if (width < 0 || height < 0)
		return 0;
	return width * height;
}

float sq(Point2f p1, Point2f p2) 
{
	return (p2.x - p1.x) * (p2.y - p1.y);
}

Mat match(Mat img, Mat templ, Mat img_display, string name, Point2f p1, Point2f p2)
{
	int result_cols = img.cols - templ.cols + 1;
	int result_rows = img.rows - templ.rows + 1;
	Mat result;
	result.create(result_rows, result_cols, CV_32FC1);
	matchTemplate(img, templ, result, TM_SQDIFF_NORMED);
	double minVal; double maxVal; Point minLoc; Point maxLoc;
	Point matchLoc;
	minMaxLoc(result, &minVal, &maxVal, &minLoc, &maxLoc, Mat());
	normalize(result, result, 0, 1, NORM_MINMAX, -1, Mat());
	matchLoc = minLoc;
	rectangle(img_display, p1, p2, CV_RGB(255, 0, 0), 2, 8, 0);
	rectangle(img_display, matchLoc, Point(matchLoc.x + templ.cols, matchLoc.y + templ.rows), CV_RGB(0, 255, 0), 2, 8, 0);
	float accuracy = s(p1.x, p1.y, p2.x, p2.y, matchLoc.x, matchLoc.y, matchLoc.x + templ.cols, matchLoc.y + templ.rows) / sq(p1, p2);
	name = name + ":";
	string acc = to_string((int)(accuracy * 100)) + "%";
	putText(img_display, name, Point2f(matchLoc.x+10, matchLoc.y+30), FONT_HERSHEY_TRIPLEX, 1, CV_RGB(0, 255, 0), 1, 23);
	putText(img_display, acc, Point2f(matchLoc.x + 10, matchLoc.y + 50), FONT_HERSHEY_TRIPLEX, 1, CV_RGB(0, 255, 0), 1, 23);
	return img_display;
}

int main()
{

	Point2f arr1[4] = { Point2f(551, 872), Point2f(199, 49), Point2f(6, 48), Point2f(13,1056)};
	Point2f arr2[4] = { Point2f(551+82, 872+69), Point2f(199 + 254, 48 + 73), Point2f(6+88, 48+67),Point2f(13+111,1056+74) };
	string global_path = "D:/Repos/myhm/kursdata/";
	string files_arr[4] = {"save.jpg","logo.jpg","story.jpg", "home.jpg"};
	Mat img2 = imread("D:/Repos/myhm/kursdata/2.jpg", IMREAD_COLOR);
	Mat img3 = imread("D:/Repos/myhm/kursdata/3.jpg", IMREAD_COLOR);
	Mat img4 = imread("D:/Repos/myhm/kursdata/4.jpg", IMREAD_COLOR);
	Mat img5 = imread("D:/Repos/myhm/kursdata/5.jpg", IMREAD_COLOR);
	Mat img6 = imread("D:/Repos/myhm/kursdata/6.jpg", IMREAD_COLOR);
	Mat data[5] = { img2, img3, img4, img5, img6 };
	for (int j = 0; j < 5; j++)
	{
		Mat img_display;
		data[j].copyTo(img_display);
		for (int i = 0; i < 4; i++)
		{
			string path = global_path + files_arr[i];
			Mat templ = imread(path, IMREAD_COLOR);
			img_display = match(data[j], templ, img_display, files_arr[i].substr(0, files_arr[i].size() - 3), arr1[i], arr2[i]);
		}
		imwrite("img_display"+to_string(j)+".jpg", img_display);
	}
	return 0;
}



void mmain()
{
	Point2f logo1 = Point2f(199, 49);
	Point2f logo2 = Point2f(199+254, 48+73);
	Mat img = imread("D:/Repos/myhm/kursdata/2.jpg", IMREAD_COLOR);
	Mat templ = imread("D:/Repos/myhm/kursdata/logo.jpg", IMREAD_COLOR);
	Mat img_display;
	img.copyTo(img_display);
	match(img, templ, img_display, "logo" ,logo1, logo2);
	//int result_cols = img.cols - templ.cols + 1;
	//int result_rows = img.rows - templ.rows + 1;
	//Mat result;
	//result.create(result_rows, result_cols, CV_32FC1);
	//matchTemplate(img, templ, result, TM_SQDIFF_NORMED);
	//double minVal; double maxVal; Point minLoc; Point maxLoc;
	//Point matchLoc;
	//minMaxLoc(result, &minVal, &maxVal, &minLoc, &maxLoc, Mat());
	//normalize(result, result, 0, 1, NORM_MINMAX, -1, Mat());
	//matchLoc = minLoc;
	//rectangle(img_display, matchLoc, Point(matchLoc.x + templ.cols, matchLoc.y + templ.rows), CV_RGB(255, 0, 0), 2, 8, 0);
	//rectangle(result, matchLoc, Point(matchLoc.x + templ.cols, matchLoc.y + templ.rows), CV_RGB(255, 0, 0), 2, 8, 0);
	imwrite("img_display.jpg", img_display);
	//imwrite("result.jpg", result);
}
